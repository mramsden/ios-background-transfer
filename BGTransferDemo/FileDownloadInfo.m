//
//  FileDownloadInfo.m
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import "FileDownloadInfo.h"

@interface FileDownloadInfo ()

@property (nonatomic,strong) NSProgress *downloadProgress;
@property (nonatomic,strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic,readwrite) BOOL downloading;
@property (nonatomic,readwrite) BOOL downloaded;

@end

@implementation FileDownloadInfo

- (instancetype)initWithFileTitle:(NSString *)fileTitle downloadSource:(NSURL *)downloadSource {
    if (self = [super init]) {
        self.fileTitle = fileTitle;
        self.downloadSource = downloadSource;
        self.downloadProgress = [NSProgress progressWithTotalUnitCount:-1];
    }
    
    return self;
}

@end
