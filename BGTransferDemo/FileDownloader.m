//
//  FileDownloader.m
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import "FileDownloader.h"

NSString * const FileDownloaderDownloadResumedNotification = @"com.smilemachine.FileDownloaderDownloadResumed";
NSString * const FileDownloaderDownloadSuspendedNotification = @"com.smilemachine.FileDownloaderDownloadSuspended";
NSString * const FileDownloaderDownloadCompletedNotification = @"com.smilemachine.FileDownloaderDownloadCompleted";

NSString * const FileDownloaderFileDownloadKey = @"com.smilemachine.FileDownloaderFileDownload";

@interface FileDownloadInfo (FileDownloader)

@property (nonatomic,strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic,readwrite) BOOL downloaded;
@property (nonatomic,readwrite) BOOL downloading;

@end

@interface FileDownloader () <NSURLSessionDataDelegate>

@property (nonatomic,strong) NSURLSession *downloadSession;
@property (nonatomic,strong) NSMutableArray *queuedDownloads;
@property (nonatomic,strong) NSURL *documentsDirectoryPath;
@property (nonatomic,strong) NSOperationQueue *downloadProcessorQueue;

@end

@implementation FileDownloader

- (instancetype)init {
    if (self = [super init]) {
        self.downloadProcessorQueue = [[NSOperationQueue alloc] init];
        
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.smilemachine.BGTransferDemo"];
        sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
        self.downloadSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:self.downloadProcessorQueue];
        self.queuedDownloads = [NSMutableArray array];
        
        self.documentsDirectoryPath = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    }
    
    return self;
}

- (NSArray *)downloads {
    return [self.queuedDownloads copy];
}

- (void)startAllDownloads {
    [self.queuedDownloads enumerateObjectsUsingBlock:^(FileDownloadInfo *fileDownloadInfo, NSUInteger idx, BOOL *stop) {
        [self startDownloadTask:fileDownloadInfo];
    }];
}

- (void)pauseAllDownloads {
    [self.queuedDownloads enumerateObjectsUsingBlock:^(FileDownloadInfo *fileDownloadInfo, NSUInteger idx, BOOL *stop) {
        [self pauseDownloadTask:fileDownloadInfo];
    }];
}

- (void)startPauseDownloadAtIndex:(NSInteger)index {
    FileDownloadInfo *fileDownloadInfo = self.queuedDownloads[index];
    if (fileDownloadInfo.downloading) {
        [self pauseDownloadTask:fileDownloadInfo];
    } else {
        [self startDownloadTask:fileDownloadInfo];
    }
}

- (FileDownloadInfo *)addDownloadWithTitle:(NSString *)title source:(NSURL *)source {
    FileDownloadInfo *fileDownloadInfo = [[FileDownloadInfo alloc] initWithFileTitle:title downloadSource:source];
    
    fileDownloadInfo.localSource = [self.documentsDirectoryPath URLByAppendingPathComponent:source.lastPathComponent];
    fileDownloadInfo.downloaded = [[NSFileManager defaultManager] fileExistsAtPath:fileDownloadInfo.localSource.path];
    
    [self.queuedDownloads addObject:fileDownloadInfo];
    return fileDownloadInfo;
}

- (void)deleteAllDownloadedFiles {
    [self pauseAllDownloads];
    NSDirectoryEnumerator *directoryEnumerator = [[NSFileManager defaultManager] enumeratorAtURL:self.documentsDirectoryPath includingPropertiesForKeys:nil options:0 errorHandler:nil];
    for (NSURL *fileURL in directoryEnumerator) {
        [[NSFileManager defaultManager] removeItemAtURL:fileURL error:nil];
    }
    [self.queuedDownloads enumerateObjectsUsingBlock:^(FileDownloadInfo *fileDownloadInfo, NSUInteger idx, BOOL *stop) {
        fileDownloadInfo.downloaded = NO;
    }];
}

#pragma mark - NSURLSessionDownloadDelegate methods -

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    [self.queuedDownloads enumerateObjectsUsingBlock:^(FileDownloadInfo *downloadInfo, NSUInteger idx, BOOL *stop) {
        if ([downloadInfo.downloadTask isEqual:downloadTask]) {
            NSError *error;
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if ([fileManager fileExistsAtPath:downloadInfo.localSource.path]) {
                [fileManager removeItemAtURL:downloadInfo.localSource error:nil];
            }
            
            downloadInfo.downloadProgress.completedUnitCount = downloadInfo.downloadProgress.totalUnitCount;
            downloadInfo.downloadTask = nil;
            downloadInfo.downloading = NO;
            BOOL success = [fileManager copyItemAtURL:location toURL:downloadInfo.localSource error:&error];
            if (success) {
                downloadInfo.downloaded = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloaderDownloadCompletedNotification object:self userInfo:@{FileDownloaderFileDownloadKey: downloadInfo}];
            }
            
            *stop = YES;
        }
    }];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    [self.queuedDownloads enumerateObjectsUsingBlock:^(FileDownloadInfo *downloadInfo, NSUInteger idx, BOOL *stop) {
        if ([downloadInfo.downloadTask isEqual:downloadTask]) {
            if (totalBytesExpectedToWrite != NSURLSessionTransferSizeUnknown) {
                NSProgress *progress = downloadInfo.downloadProgress;
                progress.completedUnitCount = totalBytesWritten;
                progress.totalUnitCount = totalBytesExpectedToWrite;
            }
            *stop = YES;
        }
    }];
}

#pragma mark - Helper methods -

- (void)startDownloadTask:(FileDownloadInfo *)fileDownloadInfo {
    if (!fileDownloadInfo.downloading) {
        fileDownloadInfo.downloading = YES;
        if (!fileDownloadInfo.downloadTask) {
            fileDownloadInfo.downloadTask = [self.downloadSession downloadTaskWithURL:fileDownloadInfo.downloadSource];
        }
        [fileDownloadInfo.downloadTask resume];
        [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloaderDownloadResumedNotification object:self userInfo:@{FileDownloaderFileDownloadKey: fileDownloadInfo}];
    }
}

- (void)pauseDownloadTask:(FileDownloadInfo *)fileDownloadInfo {
    if (fileDownloadInfo.downloading) {
        fileDownloadInfo.downloading = NO;
        [fileDownloadInfo.downloadTask suspend];
        [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloaderDownloadSuspendedNotification object:self userInfo:@{FileDownloaderFileDownloadKey: fileDownloadInfo}];
    }
}

@end
