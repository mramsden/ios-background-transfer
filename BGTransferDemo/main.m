//
//  main.m
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
