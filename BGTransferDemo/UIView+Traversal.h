//
//  UIView+Traversal.h
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Traversal)

- (UITableViewCell *)containingTableViewCell;

@end
