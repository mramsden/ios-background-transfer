//
//  FileDownloadInfo.h
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileDownloadInfo : NSObject

@property (nonatomic,copy) NSString *fileTitle;
@property (nonatomic,copy) NSURL *downloadSource;
@property (nonatomic,copy) NSURL *localSource;
@property (nonatomic,readonly) NSProgress *downloadProgress;
@property (nonatomic,readonly) BOOL downloading;
@property (nonatomic,readonly) BOOL downloaded;

- (instancetype)initWithFileTitle:(NSString *)fileTitle downloadSource:(NSURL *)downloadSource;

@end
