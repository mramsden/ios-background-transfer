//
//  ViewController.m
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import "ViewController.h"
#import "FileDownloadInfo.h"
#import "FileDownloadTableViewCell.h"
#import "UIView+Traversal.h"
#import "FileDownloader.h"

@import WebKit;

@interface ViewController () <UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate,FileDownloadTableViewCellDelegate>

@property (nonatomic,weak) IBOutlet UITableView *downloadsTableView;
@property (nonatomic,strong) FileDownloader *fileDownloader;

@end

@implementation ViewController {
    id<NSObject> _downloadCompleteObserver;
}

static void *ProgressObserverContext = &ProgressObserverContext;

#pragma mark - View lifecycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeFileDownloader];
    self.downloadsTableView.delegate = self;
    self.downloadsTableView.dataSource = self;
    self.downloadsTableView.rowHeight = 60.0f;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _downloadCompleteObserver = [[NSNotificationCenter defaultCenter] addObserverForName:FileDownloaderDownloadCompletedNotification object:self.fileDownloader queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        FileDownloadInfo *fileDownloadInfo = note.userInfo[FileDownloaderFileDownloadKey];
        NSIndexPath *indexPath = [self indexPathOfFileDownload:fileDownloadInfo];
        [self.downloadsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    [self.fileDownloader.downloads enumerateObjectsUsingBlock:^(FileDownloadInfo *fileDownloadInfo, NSUInteger idx, BOOL *stop) {
        [self addDownloadProgressObserver:fileDownloadInfo];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.fileDownloader.downloads enumerateObjectsUsingBlock:^(FileDownloadInfo *fileDownloadInfo, NSUInteger idx, BOOL *stop) {
        [self removeDownloadProgressObserver:fileDownloadInfo];
    }];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_downloadCompleteObserver name:FileDownloaderDownloadCompletedNotification object:self.fileDownloader];
    
    [super viewDidDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UI Action Handlers -

- (IBAction)startAllDownloads:(id)sender {
    [self.fileDownloader startAllDownloads];
    [self.downloadsTableView reloadData];
}

- (IBAction)deleteAllDownloadedFiles:(id)sender {
    [self.fileDownloader deleteAllDownloadedFiles];
    [self.downloadsTableView reloadData];
}

#pragma mark - UITableViewDataSource methods -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fileDownloader.downloads.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FileDownloadTableViewCell *cell = (FileDownloadTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DownloadCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell bindFileDownloadInfo:self.fileDownloader.downloads[indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate methods -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FileDownloadInfo *fileDownloadInfo = self.fileDownloader.downloads[indexPath.row];
    
    if (!fileDownloadInfo.downloaded) {
        [self.fileDownloader startPauseDownloadAtIndex:indexPath.row];
        [self.downloadsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        UIDocumentInteractionController *documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileDownloadInfo.localSource];
        documentInteractionController.name = fileDownloadInfo.fileTitle;
        documentInteractionController.delegate = self;
        [documentInteractionController presentPreviewAnimated:YES];
    }
}

#pragma mark - UIDocumentInteractionControllerDelegate methods -

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self.navigationController;
}

#pragma mark - FileDownloadTableViewCellDelegate methods -

- (void)fileDownloadTableViewCellStartPauseButtonTapped:(FileDownloadTableViewCell *)cell {
    NSIndexPath *indexPath = [self.downloadsTableView indexPathForCell:cell];
    [self.fileDownloader startPauseDownloadAtIndex:indexPath.row];
    [self.downloadsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - KVO -

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (ProgressObserverContext == context) {
        NSProgress *progress = object;
        
        [self.fileDownloader.downloads enumerateObjectsUsingBlock:^(FileDownloadInfo *fileDownloadInfo, NSUInteger idx, BOOL *stop) {
            if (fileDownloadInfo.downloadProgress == progress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progress.fractionCompleted < 1.0) {
                        FileDownloadTableViewCell *cell = (FileDownloadTableViewCell *)[self.downloadsTableView cellForRowAtIndexPath:[self indexPathOfFileDownload:fileDownloadInfo]];
                        [cell updateProgress:progress];
                    }
                });
                *stop = YES;
            }
        }];
    }
}

#pragma mark - Helpers -

- (void)initializeFileDownloader {
    self.fileDownloader = [[FileDownloader alloc] init];
    [self.fileDownloader addDownloadWithTitle:@"iOS Programming Guide" source:[NSURL URLWithString:@"https://developer.apple.com/library/ios/documentation/iphone/conceptual/iphoneosprogrammingguide/iphoneappprogrammingguide.pdf"]];
    [self.fileDownloader addDownloadWithTitle:@"Networking Overview" source:[NSURL URLWithString:@"https://developer.apple.com/library/ios/documentation/NetworkingInternetWeb/Conceptual/NetworkingOverview/NetworkingOverview.pdf"]];
    [self.fileDownloader addDownloadWithTitle:@"AV Foundation" source:[NSURL URLWithString:@"https://developer.apple.com/library/ios/documentation/AudioVideo/Conceptual/AVFoundationPG/AVFoundationPG.pdf"]];
    [self.fileDownloader addDownloadWithTitle:@"iPhone User Guide" source:[NSURL URLWithString:@"http://manuals.info.apple.com/MANUALS/1000/MA1565/en_US/iphone_user_guide.pdf"]];
}

- (void)addDownloadProgressObserver:(FileDownloadInfo *)fileDownloadInfo {
    [fileDownloadInfo.downloadProgress addObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) options:0 context:ProgressObserverContext];
}

- (void)removeDownloadProgressObserver:(FileDownloadInfo *)fileDownloadInfo {
    [fileDownloadInfo.downloadProgress removeObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted))];
}

- (NSIndexPath *)indexPathOfFileDownload:(FileDownloadInfo *)fileDownloadInfo {
    return [NSIndexPath indexPathForRow:[self.fileDownloader.downloads indexOfObject:fileDownloadInfo] inSection:0];
}

@end
