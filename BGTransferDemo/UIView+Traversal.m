//
//  UIView+Traversal.m
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import "UIView+Traversal.h"

@implementation UIView (Traversal)

- (UITableViewCell *)containingTableViewCell {
    UIView *view = self;
    while (view.superview != nil && ![view isKindOfClass:UITableViewCell.class]) {
        view = view.superview;
    }
    
    if ([view isKindOfClass:UITableViewCell.class]) {
        return (UITableViewCell *) view;
    }
    
    return nil;
}

@end
