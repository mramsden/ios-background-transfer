//
//  FileDownloader.h
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileDownloadInfo.h"

extern NSString * const FileDownloaderDownloadResumedNotification;
extern NSString * const FileDownloaderDownloadSuspendedNotification;
extern NSString * const FileDownloaderDownloadCompletedNotification;

extern NSString * const FileDownloaderFileDownloadKey;

@interface FileDownloader : NSObject

@property (nonatomic,readonly) NSArray *downloads;

- (void)startAllDownloads;
- (void)startPauseDownloadAtIndex:(NSInteger)index;
- (FileDownloadInfo *)addDownloadWithTitle:(NSString *)title source:(NSURL *)source;
- (void)deleteAllDownloadedFiles;

@end
