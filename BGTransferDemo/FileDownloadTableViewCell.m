//
//  FileDownloadTableViewCell.m
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import "FileDownloadTableViewCell.h"
#import "FileDownloadInfo.h"

@interface FileDownloadTableViewCell ()

@property (nonatomic,weak) IBOutlet UILabel *titleLabel;
@property (nonatomic,weak) IBOutlet UIProgressView *downloadProgressView;
@property (nonatomic,strong) FileDownloadInfo *fileDownloadInfo;
@property (nonatomic) BOOL updatingProgress;

@end

@implementation FileDownloadTableViewCell {
    LLACircularProgressView *_circularProgressView;
}

- (void)bindFileDownloadInfo:(FileDownloadInfo *)fileDownloadInfo {
    self.fileDownloadInfo = fileDownloadInfo;
    
    self.titleLabel.text = self.fileDownloadInfo.fileTitle;
    
    if ((!self.fileDownloadInfo.downloading && !self.fileDownloadInfo.downloaded) || (!self.fileDownloadInfo.downloading && self.fileDownloadInfo.downloaded)) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
    } else {
        self.accessoryView = self.circularProgressView;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

- (void)updateProgress:(NSProgress *)progress {
    [self.circularProgressView setProgress:progress.fractionCompleted animated:YES];
}

- (LLACircularProgressView *)circularProgressView {
    if (_circularProgressView == nil) {
        _circularProgressView = [[LLACircularProgressView alloc] initWithFrame:CGRectMake(0, 0, 33.0f, 33.0f)];
        [_circularProgressView addTarget:self action:@selector(startPauseButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _circularProgressView;
}

- (void)startPauseButtonTapped:(id)sender {
    [self.delegate fileDownloadTableViewCellStartPauseButtonTapped:self];
}

@end
