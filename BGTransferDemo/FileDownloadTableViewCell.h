//
//  FileDownloadTableViewCell.h
//  BGTransferDemo
//
//  Created by Marcus Ramsden on 22/03/2015.
//  Copyright (c) 2015 Smile Machine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LLACircularProgressView/LLACircularProgressView.h>

@class FileDownloadInfo;
@class FileDownloadTableViewCell;

@protocol FileDownloadTableViewCellDelegate <NSObject>

- (void)fileDownloadTableViewCellStartPauseButtonTapped:(FileDownloadTableViewCell *)cell;

@end

@interface FileDownloadTableViewCell : UITableViewCell

@property (nonatomic) id<FileDownloadTableViewCellDelegate> delegate;

- (void)bindFileDownloadInfo:(FileDownloadInfo *)fileDownloadInfo;
- (void)updateProgress:(NSProgress *)progress;

@end
